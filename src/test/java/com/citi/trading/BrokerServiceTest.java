package com.citi.trading;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;




/**
 * Unit test for the {@link BrokerService} class.
 * We provide a mock {@link Market} to accept outbound orders,
 * and we simulate fills, partial fills, and rejections by calling the 
 * registered <strong>Consumer</strong> directly.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@DirtiesContext
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
public class BrokerServiceTest {
	private static Trade pendingTrade;
	private static Consumer<Trade> pendingCallback;
	
	@Configuration
	
	public static class Config{
		@Bean
		@Scope("prototype")
		public BrokerService BrokerService() {
			return new BrokerService();
		}
		
		@Bean
		public OrderPlacer MockMarket2 () {
			//MockMarket mockMarket = new MockMarket();		
			return new MockMarket();
		}
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			Investor investor = Mockito.mock(Investor.class);
		
			return new Investor();
		}
	}
	
	private static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			pendingTrade = order;
			pendingCallback = callback;
		}
	}
	
	@Autowired
	BrokerService brokerService;
	
	
	@Test
	public void getAll() {
		List<Investor> investors = brokerService.getAll();
		assertThat(investors.size(),is(2));
		
		assertThat(investors.get(0).getId(),is(1));
		assertThat(investors.get(1).getId(),is(2));
		}
	
	@Test
	public void testGetById() {
		Investor investor = brokerService.getByID(2).getBody();
		
		assertThat(investor.getId(), is(2));
		assertThat(investor.getCash(),closeTo(1000.0,0000.1));
		assertThat(investor.getPortfolio(), hasEntry("MRK", 100));
	}
	
	@Test
	public void testOpenAccount() {
		brokerService.openAccount(5000);
		int id=brokerService.getNextId()-1;
		assertThat(brokerService.getAll().size(), is(3));
		assertThat(brokerService.getByID(id).getBody().getCash(), closeTo(5000, 0.00001));
		assertThat(brokerService.getByID(id).getBody().getPortfolio(), is(notNullValue()));
	}
	
	@Test 
	public void closeAccount() {
		Investor investor = brokerService.getByID(1).getBody();
		brokerService.closeAccount(1);
		assertThat(brokerService.getAll(),not(hasItem(investor)));
	}
	
	//TODO

	/*
	@Test(expected=IllegalStateException.class)
	public void testCloseInvalidAccount(){
		brokerService.closeAccount(99);
	}*/
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidCash() {
		brokerService.openAccount(-1000)
;	}


}
