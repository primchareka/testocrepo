package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for the {@link Investor} class.
 * We provide a mock {@link Market} to accept outbound orders,
 * and we simulate fills, partial fills, and rejections by calling the 
 * registered <strong>Consumer</strong> directly.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=InvestorTest.Config.class)
public class InvestorTest {

	private static Trade pendingTrade;
	private static Consumer<Trade> pendingCallback;

	
	/**
	 * The test configuration replaces the usual Market and Investor objects
	 * with mock objects.
	 */
	@Configuration
	public static class Config{
		@Bean
		public OrderPlacer MockMarket () {
			//MockMarket mockMarket = new MockMarket();		
			return new MockMarket();
		}
		
		@Bean
		public Investor investor() {
			//Investor investor = Mockito.mock(Investor.class);
		
			return new Investor();
		}
		
	}
	
	@Autowired
	private Investor investor;
	
	private static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			pendingTrade = order;
			pendingCallback = callback;
		}
	}


	
	private void buyAndConfirm(String stock, int shares, double price) {
		investor.buy(stock, shares, price);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndConfirm(String stock, int shares, double price, int partConfirmed) {
		investor.buy(stock, shares, price);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndReject(String stock, int shares, double price) {
		investor.buy(stock, shares, price);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price) {
		investor.sell(stock, shares, price);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price, int partConfirmed) {
		investor.sell(stock, shares, price);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndReject(String stock, int shares, double price) {
		investor.sell(stock, shares, price);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	@Test
	public void testBuy() {
		investor.setCash(40000);		
		investor.setPortfolio(new HashMap<String, Integer>());

		buyAndConfirm("KHC", 100, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(100)));
		assertThat(investor.getCash(), closeTo(30000.0, 0.0001));
	}

	@Test
	public void testSell() {
		investor.setCash(0);
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		investor.setCash(0);
		sellAndConfirm("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9000)));
		assertThat(investor.getCash(), closeTo(100000.0, 0.0001));
	}

	@Test
	public void testSellOut() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setCash(0);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 10000, 100);
		assertThat(investor.getPortfolio(), not(hasKey(equalTo("MSFT"))));
	}

	@Test(expected=IllegalStateException.class)
	public void testBuyWithInsufficientCash() {
		investor.setCash(4000);
		buyAndConfirm("KHC", 100, 100);
	}

	@Test(expected=IllegalStateException.class)
	public void testSellWithInsufficientShares() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 100);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
	}

	// ******
	@Test(expected=IllegalStateException.class)
	public void testSellWithNoShares() {
		investor.setCash(0);
		Map<String,Integer> starter = new HashMap<>();
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
	}
	
	@Test
	public void testBuyPartial() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 100, 100, 80);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(80)));
		assertThat(investor.getCash(), closeTo(32000.0, 0.0001));
	}
	
	@Test
	public void testBuyRejected() {
		investor.setCash(40000);
		Map<String,Integer> starter = new HashMap<>();

		investor.setPortfolio(starter);
		buyAndReject("KHC", 100, 100);
		assertThat(investor.getPortfolio(), not(hasKey("KHC")));
		assertThat(investor.getCash(), closeTo(40000.0, 0.0001));
	}
	
	@Test
	public void testSellPartial() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setCash(0);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100, 500);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9500)));
		assertThat(investor.getCash(), closeTo(50000.0, 0.0001));
	}

	@Test
	public void testSellRejected() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		investor.setCash(0);
		sellAndReject("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));
		assertThat(investor.getCash(), closeTo(0.0, 0.0001));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidCash() {
		investor.setCash(-40000);
	}

	// ******
	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidShareCount() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 0);
		investor.setPortfolio(starter);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBuyInvalidShareCount() {
		buyAndConfirm("KHC", 0, 100);
	}
}
